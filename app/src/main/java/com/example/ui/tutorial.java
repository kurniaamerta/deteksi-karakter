package com.example.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class tutorial extends AppCompatActivity {

    VideoView videoplay;
    MediaController mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_tutorial);

        videoplay = (VideoView) findViewById(R.id.videoView2);
        mController = new MediaController(this);

        videoplay.setVideoPath("android.resource://"+getPackageName()+"/"+R.raw.test_vid_abc);
        mController.setAnchorView(videoplay);
        videoplay.setMediaController(mController);

        videoplay.start();

    }
}
