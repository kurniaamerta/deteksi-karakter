package com.example.ui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class playgame extends AppCompatActivity {
    private ImageButton imagebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_playgame);
        final MediaPlayer b_clickb = MediaPlayer.create(this,R.raw.b_click);


        imagebutton = findViewById(R.id.button_latihan);
        imagebutton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                b_clickb.start();
                play_angka();
            }
        });

        imagebutton = findViewById(R.id.button_ujian);
        imagebutton.setOnClickListener(new  View.OnClickListener(){
            @Override
            public void onClick(View v){
                b_clickb.start();
                play_ujian();
            }
        });
    }

    private void play_angka(){
        Intent intent = new Intent(this, play_angka.class);
        startActivity(intent);
    }
    private void play_ujian(){
        Intent intent = new Intent(this, play_ujian.class);
        startActivity(intent);
    }


}
