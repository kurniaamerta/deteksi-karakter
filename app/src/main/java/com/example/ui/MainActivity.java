package com.example.ui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    private ImageButton imagebutton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);

        final MediaPlayer b_clickb = MediaPlayer.create(this,R.raw.b_click);
        final MediaPlayer bgm = MediaPlayer.create(this, R.raw.bgm_test);

        bgm.setLooping(true);
        bgm.start();

        imagebutton = findViewById(R.id.b_start);
        imagebutton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                b_clickb.start();
                start_game();
            }
        });

        imagebutton = findViewById(R.id.b_tutorial);
        imagebutton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                b_clickb.start();
                bgm.stop();
                open_tutorial();
            }
        });

//        imagebutton = findViewById(R.id.b_exit;
//        imagebutton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v){
//                ;
//            }
//        });


    }

    private void open_tutorial() {
        Intent intent = new Intent(this, tutorial.class);
        startActivity(intent);
    }
    private void start_game(){
        Intent intent = new Intent(this, playgame.class);
        startActivity(intent);
    }
    private void open_chapter(){
        Intent intent = new Intent(this, chapter.class);
        startActivity(intent);
    }

}
